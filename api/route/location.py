import json
import logging
import falcon
from api.route import utils
from api.route.entity.location import Location
from core.baseapimain import BaseApiMain
from core.response import Response, ResponseErrorData
from core.internalerrors import ValidateError


def parse_location(req, resp, resource, params):
    body_json = json.loads(req.stream.read().decode())
    params['location'] = Location.parse(body_json)


class RouteLocationList(BaseApiMain):
    _log = logging.getLogger('cygnus.api.route.location.list')

    def on_get(self, req, resp, route_id):
        super(RouteLocationList, self).on_get(req, resp)
        log = self._log
        params = self._params
        db = self._db

        log.debug('Getting location for route from database ...')
        locations = db.get_location_list(params['page'], params['limit'], route_id)
        log.debug('Found %s locations.', len(locations))
        response = Response.ok(locations, params)
        resp.body = json.dumps(response)
        resp.status = response.response_status

    @falcon.before(parse_location)
    def on_post(self, req, resp, route_id, location):
        super(RouteLocationList, self).on_post(req, resp)
        log = self._log
        db = self._db

        validate_errors = utils.validate_location_for_post(location)

        if validate_errors:
            response = Response.bad_request(validate_errors)
        else:
            route = db.get_route_by_id(route_id)
            if route:
                new_location = db.insert_location(location, route_id)
                if new_location:
                    response = Response.created(new_location)
                    log.debug('Created new location for route_id=%s.', route_id)
                else:
                    response = Response.internal_error()
            else:
                response = Response.not_found('route')

        if response.response_status is not falcon.HTTP_CREATED:
            log.error('Cannot insert new location for route_id=%s.', route_id)
            log.error(validate_errors)

        resp.body = json.dumps(response)
        resp.status = response.response_status


class RouteLocationOne(BaseApiMain):
    _log = logging.getLogger('cygnus.api.route.location.one')

    def on_get(self, req, resp, route_id, location_id):
        super(RouteLocationOne, self).on_get(req, resp)
        log = self._log
        params = self._params
        db = self._db

        log.debug('Getting one location from database...')
        location = db.get_location_by_id(location_id)

        if location:
            log.debug('Found location_id=%s.', location_id)
            response = Response.ok([location], params)
        else:
            log.debug('Not found location_id=%s', location_id)
            e = ResponseErrorData(ValidateError.FIELD_NOT_FOUND, 'Not found', 'location')
            response = Response.not_found([e])

        resp.body = json.dumps(response)
        resp.status = response.response_status

    def on_delete(self, req, resp, route_id, location_id):
        super(RouteLocationOne, self).on_delete(req, resp)
        log = self._log
        db = self._db

        log.debug('Deleting one location from database...')
        deleted = db.delete_location_by_id(location_id)

        if deleted:
            log.debug('Deleted location_id=%s', location_id)
            response = Response.deleted()
        else:
            log.debug('Not found location_id=%s', location_id)
            e = ResponseErrorData(ValidateError.FIELD_NOT_FOUND, 'Not found', 'location')
            response = Response.not_found([e])
            resp.body = json.dumps(response)

        resp.status = response.response_status
