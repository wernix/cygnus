from api.route.entity.location import Location
from core.internalerrors import ValidateResponseErrorData


def validate_location_for_post(location: Location):
    errors = list()

    # id field
    if 'id' in location:
        if type(location['id']) is int:
            if location['id'] >= 0:
                errors.append(
                    ValidateResponseErrorData.field_invalid_value('id',
                                                                  'ID must have value lower than zero for insert.'))
        else:
            errors.append(ValidateResponseErrorData.field_invalid_type('id', 'integer'))
    else:
        errors.append(ValidateResponseErrorData.field_not_found('id'))

    # latitude
    if 'latitude' in location:
        if type(location['latitude']) is not float:
            errors.append(ValidateResponseErrorData.field_invalid_type('latitude', 'float'))
    else:
        errors.append(ValidateResponseErrorData.field_not_found('latitude'))

    # longitude
    if 'longitude' in location:
        if type(location['longitude']) is not float:
            errors.append(ValidateResponseErrorData.field_invalid_type('longitude', 'float'))
    else:
        errors.append(ValidateResponseErrorData.field_not_found('longitude'))

    # dateCreate
    if 'dateCreate' in location:
        if type(location['dateCreate']) is not str:
            errors.append(ValidateResponseErrorData.field_invalid_type('dateCreate', 'str'))
    else:
        errors.append(ValidateResponseErrorData.field_not_found('dateCreate'))

    return errors
