from datetime import datetime
from api.user.entity.user import User


class Route(dict):

    def __init__(self, route_id: int, name: str, user: User, date_create: datetime):
        super(Route, self).__init__()
        self['id'] = route_id
        self['name'] = name
        if user is not None:
            self['user'] = user

        if type(date_create) is datetime:
            self['dateCreate'] = date_create.strftime('%Y-%m-%d %X')
        elif type(date_create) is str:
            self['dateCreate'] = date_create
