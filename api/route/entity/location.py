from datetime import datetime


class Location(dict):

    def __init__(self, location_id: int, latitude: float, longitude: float, date_create):
        super(Location, self).__init__()
        self['id'] = location_id
        self['latitude'] = latitude
        self['longitude'] = longitude

        if type(date_create) is datetime:
            self['dateCreate'] = date_create.strftime('%Y-%m-%d %X')
        elif type(date_create) is str:
            self['dateCreate'] = date_create

    @classmethod
    def parse(cls, json: dict):
        return cls(json['id'] if 'id' in json else None,
                   json['latitude'] if 'latitude' in json else None,
                   json['longitude'] if 'longitude' in json else None,
                   json['dateCreate'] if 'dateCreate' in json else None)
