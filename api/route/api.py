from api.route.route import RouteList, RouteOne
from api.route.location import RouteLocationList, RouteLocationOne
from core.baseapi import BaseApi


class RouteApi(BaseApi):

    def _setup_main(self):
        db = self._db
        self._add_route('/api/route', RouteList(db))
        self._add_route('/api/route/{route_id}', RouteOne(db))
        self._add_route('/api/route/{route_id}/location', RouteLocationList(db))
        self._add_route('/api/route/{route_id}/location/{location_id}', RouteLocationOne(db))
