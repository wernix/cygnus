import logging
import json
from core.baseapimain import BaseApiMain
from core.response import Response, ResponseErrorData
from core.internalerrors import ValidateError


class RouteList(BaseApiMain):
    _log = logging.getLogger('cygnus.api.route.list')

    def on_get(self, req, resp):
        super(RouteList, self).on_get(req, resp)
        log = self._log
        params = self._params
        db = self._db

        log.debug('Getting route list from database...')
        routes = db.get_route_list(params['page'], params['limit'])
        log.debug('Found %d routes.', len(routes))
        response = Response.ok(routes, params)
        resp.body = json.dumps(response)
        resp.status = response.response_status


class RouteOne(BaseApiMain):
    _log = logging.getLogger('cygnus.api.route.one')

    def on_get(self, req, resp, route_id):
        super(RouteOne, self).on_get(req, resp)
        log = self._log
        db = self._db

        log.debug('Getting one route object from database...')
        route = db.get_route_by_id(route_id)

        if route:
            log.debug('Found route_id=%s', route_id)
            response = Response.ok([route], None)
        else:
            log.debug('Not found route_id=%s', route_id)
            e = ResponseErrorData(ValidateError.FIELD_NOT_FOUND, 'Not found', 'route')
            response = Response.not_found([e])

        resp.body = json.dumps(response)
        resp.status = response.response_status
