from datetime import datetime


class User(dict):

    def __init__(self, user_id: int, login: str, password, date_create):
        super(User, self).__init__()
        self['id'] = user_id
        self['login'] = login

        if type(password) is str:
            self['password'] = password

        if type(date_create) is datetime:
            self['dateCreate'] = date_create.strftime('%Y-%m-%d %X')
        elif type(date_create) is str:
            self['dateCreate'] = date_create
