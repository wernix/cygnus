from api.user.user import UserList, UserOne
from core.baseapi import BaseApi


class UserApi(BaseApi):

    def _setup_main(self):
        db = self._db
        self._add_route('/api/user', UserList(db))
        self._add_route('/api/user/{user_id}', UserOne(db))
