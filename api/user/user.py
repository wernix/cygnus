import logging
import json
from core.response import Response, ResponseErrorData
from core.baseapimain import BaseApiMain
from core.internalerrors import ValidateError


class UserList(BaseApiMain):
    _log = logging.getLogger('cygnus.api.user.list')

    def on_get(self, req, resp):
        super(UserList, self).on_get(req, resp)
        params = self._params
        log = self._log
        db = self._db

        log.debug('Getting user list from database...')
        users = db.get_user_list(params['page'], params['limit'])
        log.debug('Found %d users.', len(users))
        response = Response.ok(users, params)

        resp.body = json.dumps(response)
        resp.status = response.response_status


class UserOne(BaseApiMain):
    _log = logging.getLogger('cygnus.api.user.id')

    def on_get(self, req, resp, user_id):
        super(UserOne, self).on_get(req, resp)
        log = self._log
        db = self._db

        log.debug('Getting one user object from database...')
        user = db.get_user_by_id(user_id)
        if user:
            log.debug('Found user_id=%s', user_id)
            response = Response.ok([user], None)
        else:
            log.debug('Not found user_id=%s', user_id)
            e = ResponseErrorData(ValidateError.FIELD_NOT_FOUND, 'Not found', 'user')
            response = Response.not_found([e])

        resp.body = json.dumps(response)
        resp.status = response.response_status
