#!/usr/bin/env bash
python3 -t -m unittest \
    tests/core/auth.py \
    tests/core/responsebase.py \
    tests/core/session.py \
    tests/api/user/user.py \
    tests/api/route/route.py \
    tests/api/route/location.py
