#!/usr/bin/env bash
gunicorn -b 0.0.0.0:8000 \
--access-logfile log/cygnus.access.log \
--access-logformat '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s' \
--error-logfile log/cygnus.error.log \
--reload \
cygnus:app