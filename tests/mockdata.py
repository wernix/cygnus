from core.utils import authutils


admins = [
    {
        'id': 1,
        'login': 'admin',
        'password': authutils.sha512_encode('1234'),
        'date_create': '2017-03-03 08:01:12'
    }
]

users = [
    {
        'id': 2,
        'login': 'demo',
        'password': authutils.sha512_encode('asdcv@311.'),
        'date_create': '2017-03-03 08:02:22'
    },
    {
        'id': 3,
        'login': 'demo1',
        'password': authutils.sha512_encode('asdcv@311.'),
        'date_create': '2017-03-03 08:02:51'
    }
]

routes = [
    {
        'id': 1,
        'name': 'Trasa 1',
        'user_id': 1,
        'date_create': '2017-03-03 09:01:12'
    },
    {
        'id': 2,
        'name': 'Trasa 2',
        'user_id': 1,
        'date_create': '2017-03-03 09:02:01'
    },
    {
        'id': 3,
        'name': 'Trasa 3',
        'user_id': 1,
        'date_create': '2017-03-03 09:05:59'
    }
]

locations_route_1 = [
    {
        'id': 1,
        'route_id': 1,
        'latitude': 1.123,
        'longitude': 2.123,
        'date_create': '2017-03-03 09:12:01'
    },
    {
        'id': 2,
        'route_id': 1,
        'latitude': 4.123,
        'longitude': 2.223,
        'date_create': '2017-03-03 09:13:01'
    }
]

locations_route_2_new = [
    {
        'id': -1,
        'route_id': 1,
        'latitude': 1.123,
        'longitude': 2.123,
        'date_create': '2017-03-03 09:12:01'
    },
    {
        'id': -1,
        'route_id': 1,
        'latitude': 4.123,
        'longitude': 2.223,
        'date_create': '2017-03-03 09:13:01'
    },
    {
        'id': 12,
        'route_id': 1,
        'latitude': 4.123,
        'longitude': 2.223,
        'date_create': '2017-03-03 09:13:01'
    }
]
