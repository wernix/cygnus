from unittest import main
from core.test.testapi import TestApi


class TestAuth(TestApi):

    def test_auth_by_cookie(self):
        get_result = self.simulate_request(method='GET', path='/api/user', headers=self.get_logged_admin_headers())
        self.assertEqual(200, get_result.status_code)
        self.assertEqual(get_result.json['status'], 'success')
        print('TestAuthByCookie ok')

    def test_auth_by_header(self):
        headers = dict()
        headers['Authorization'] = 'Basic YWRtaW46MTIzNA=='
        get_result = self.simulate_request(method='GET', path='/api/user', headers=headers)
        self.assertEqual(200, get_result.status_code)
        self.assertEqual(get_result.json['status'], 'success')
        print('TestAuthByHeader ok')

    def test_logout_by_post(self):
        headers = self.get_logged_admin_headers()
        get_result = self.simulate_request(method='GET', path='/api/user', headers=headers)
        self.assertEqual(200, get_result.status_code)
        post_result = self.simulate_request(method='POST', path='/logout', headers=headers)
        self.assertEqual(200, post_result.status_code)
        get_result = self.simulate_request(method='GET', path='/api/user', headers=headers)
        self.assertNotEqual(200, get_result.status_code)
        print('TestLogoutByPOST ok')

    def test_logout_by_get(self):
        headers = self.get_logged_admin_headers()
        get_result = self.simulate_request(method='GET', path='/api/user', headers=headers)
        self.assertEqual(200, get_result.status_code)
        post_result = self.simulate_request(method='GET', path='/logout', headers=headers)
        self.assertEqual(200, post_result.status_code)
        get_result = self.simulate_request(method='GET', path='/api/user', headers=headers)
        self.assertNotEqual(200, get_result.status_code)
        print('TestLogoutByGET ok')


if __name__ == '__main__':
    main()
