from unittest import main
from core.test.testapi import TestApi


class TestSession(TestApi):

    def test_create_session_cookie(self):
        result = self.simulate_request(
            method='GET',
            path='/api/user'
        )
        self.assertEqual(True, 'SessionID' in result.cookies)
        print('TestCreateSessionCookie ok')


if __name__ == '__main__':
    main()
