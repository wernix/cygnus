from unittest import main
from core.test.testapi import TestApi
from core.internalerrors import ValidateError


class TestResponseBase(TestApi):

    def test_response_object(self):
        auth_header = self.get_logged_admin_headers()
        result = self.simulate_request(method='GET', path='/api/user', headers=auth_header)
        self.assertEqual(200, result.status_code)
        self.assertEqual(True, 'status' in result.json)
        self.assertEqual(True, 'data' in result.json)
        self.assertEqual(True, 'info' in result.json)
        self.assertEqual(True, 'limit' in result.json['info'])
        self.assertEqual(True, 'page' in result.json['info'])
        self.assertEqual(True, 'nextPage' in result.json['info'])
        self.assertEqual('success', result.json['status'].lower())
        print('TestBaseResponse ok')

    def test_parse_params(self):
        auth_header = self.get_logged_admin_headers()
        query_string = 'limit=66&page=23'
        result = self.simulate_request(
            method='GET', path='/api/user', query_string=query_string, headers=auth_header)
        self.assertEqual(200, result.status_code)
        self.assertEqual(66, result.json['info']['limit'])
        self.assertEqual(23, result.json['info']['page'])
        print('TestParseParams ok')

    def test_response_error(self):
        auth_header = self.get_logged_admin_headers()
        result = self.simulate_request(method='GET', path='/api/route/87878', headers=auth_header)
        self.assertEqual(404, result.status_code)
        error = result.json['data'][0]
        self.assertEqual(ValidateError.FIELD_NOT_FOUND, error['code'])
        self.assertEqual('route', error['field'])
        print('TestBaseErrorResponse ok')


if __name__ == '__main__':
    main()
