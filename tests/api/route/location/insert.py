import json
from unittest import main
from core.test.testapi import TestApi
from core.internalerrors import ValidateError


class TestRouteLocationInsert(TestApi):

    def test_insert_success(self):
        path = '/api/route/1/location'
        expected_location = {
            "id": -1,
            "latitude": 12.2345,
            "longitude": 33.321,
            "dateCreate": "2017-04-04 12:12"
        }
        body = json.dumps(expected_location)
        print('Request:', body)
        result = self.simulate_request(method='POST', body=body, path=path, headers=self.get_logged_admin_headers())
        print('Response:', result.json)
        data = result.json['data']
        new_location = data[0]
        self.assertEqual(201, result.status_code)
        self.assertEqual('success', result.json['status'])
        self.assertEqual(1, len(data))
        self.assertTrue(new_location['id'] > 0)
        self.assertEqual(expected_location['latitude'], new_location['latitude'])
        self.assertEqual(expected_location['longitude'], new_location['longitude'])
        self.assertEqual(expected_location['dateCreate'], new_location['dateCreate'])
        print('TestInsertSuccess ok')

    def test_insert_with_invalid_two_fields_failed(self):
        path = '/api/route/1/location'
        expected_location = {
            "id": 123,
            "latitude": "12.2345",
            "longitude": 33.321,
            "dateCreate": "2017-04-04 12:12"
        }
        body = json.dumps(expected_location)
        print('Request:', body)
        result = self.simulate_request(method='POST', body=body, path=path, headers=self.get_logged_admin_headers())
        print('Response:', result.json)
        data = result.json['data']
        self.assertEqual(2, len(data))
        self.assertEqual(400, result.status_code)
        self.assertEqual('error', result.json['status'])
        print('TestInsertWithInvalidTwoFieldsFailed ok')

    def test_insert_with_invalid_value_failed(self):
        path = '/api/route/1/location'
        expected_location = {
            "id": 123,
            "latitude": 12.2345,
            "longitude": 33.321,
            "dateCreate": "2017-04-04 12:12"
        }
        body = json.dumps(expected_location)
        print('Request:', body)
        result = self.simulate_request(method='POST', body=body, path=path, headers=self.get_logged_admin_headers())
        print('Response:', result.json)
        data = result.json['data']
        error = data[0]
        self.assertEqual(400, result.status_code)
        self.assertEqual('error', result.json['status'])
        self.assertEqual(error['code'], ValidateError.FIELD_INVALID_VALUE)
        self.assertEqual(error['field'], 'id')
        print('TestInsertWithInvalidIdFailed ok')

    def test_insert_with_invalid_type_failed(self):
        path = '/api/route/1/location'
        expected_location = {
            "id": -1,
            "latitude": "12.2345",
            "longitude": 33.321,
            "dateCreate": "2017-04-04 12:12"
        }
        body = json.dumps(expected_location)
        print('Request:', body)
        result = self.simulate_request(method='POST', body=body, path=path, headers=self.get_logged_admin_headers())
        print('Response:', result.json)
        data = result.json['data']
        error = data[0]
        self.assertEqual(400, result.status_code)
        self.assertEqual('error', result.json['status'])
        self.assertEqual(error['code'], ValidateError.FIELD_INVALID_TYPE)
        self.assertEqual(error['field'], 'latitude')
        print('TestInsertWithInvalidTypeFailed ok')

if __name__ == '__main__':
    main()
