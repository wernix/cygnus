from unittest import main
from core.test.testapi import TestApi


class TestRouteList(TestApi):

    # Get List

    def test_get_list(self):
        auth_headers = self.get_logged_admin_headers()
        result = self.simulate_request(method='GET', path='/api/route', headers=auth_headers)
        self.assertEqual(200, result.status_code)
        data = result.json['data']
        route = data[0]
        self.assertEqual('success', result.json['status'])
        self.assertNotEqual(0, len(data))
        self.assertEqual(4, len(route))
        self.assertEqual(True, 'id' in route)
        self.assertEqual(True, 'name' in route)
        self.assertEqual(True, 'user' in route)
        self.assertEqual(True, 'dateCreate' in route)
        print('TestGetList ok')

    # Get One

    def test_get_one_by_id(self):
        auth_headers = self.get_logged_admin_headers()
        get_list = self.simulate_request(method='GET', path='/api/route', headers=auth_headers)
        data = get_list.json['data']
        expected = data[1]

        one_path = '/api/route/{0}'.format(expected['id'])
        get_one = self.simulate_request(method='GET', path=one_path, headers=auth_headers)
        self.assertEqual(200, get_one.status_code)
        data = get_one.json['data']
        actual = data[0]

        self.assertEqual('success', get_one.json['status'])
        self.assertEqual(1, len(data))
        self.assertEqual(expected['name'], actual['name'])
        self.assertEqual(expected['id'], actual['id'])
        self.assertEqual(expected['user']['id'], actual['user']['id'])
        self.assertEqual(expected['dateCreate'], actual['dateCreate'])
        print('TestGetOneByID ok')

    def test_get_one_by_id_not_found_failed(self):
        auth_headers = self.get_logged_admin_headers()
        one_path = '/api/route/9999'
        get_one = self.simulate_request(method='GET', path=one_path, headers=auth_headers)
        self.assertEqual(404, get_one.status_code)
        print('TestGetOneByIDNotFoundFailed ok')

if __name__ == '__main__':
    main()
