import json
from unittest import main
from core.test.testapi import TestApi
from core.internalerrors import ValidateError


class TestRouteLocationList(TestApi):

    # Get list

    def test_get_list(self):
        auth_headers = self.get_logged_admin_headers()
        get_list_routes = self.simulate_request(method='GET', path='/api/route', headers=auth_headers)
        route = get_list_routes.json['data'][0]

        path_locations = '/api/route/{0}/location'.format(route['id'])
        get_list_locations = self.simulate_request(method='GET', path=path_locations, headers=auth_headers)
        self.assertEqual(200, get_list_locations.status_code)
        data = get_list_locations.json['data']
        actual = data[0]
        self.assertEqual('success', get_list_locations.json['status'])
        self.assertEqual(4, len(actual))
        self.assertEqual(True, 'id' in actual)
        self.assertEqual(True, 'latitude' in actual)
        self.assertEqual(True, 'longitude' in actual)
        self.assertEqual(True, 'dateCreate' in actual)
        print('TestGetList ok')

    # Get One

    def test_get_one_by_id_success(self):
        auth_headers = self.get_logged_admin_headers()
        routes = self.simulate_request(method='GET', path='/api/route', headers=auth_headers)
        route = routes.json['data'][0]

        path_locations = '/api/route/{0}/location'.format(route['id'])
        get_list_locations = self.simulate_request(method='GET', path=path_locations, headers=auth_headers)
        expected = get_list_locations.json['data'][0]

        path_location = '/api/route/{0}/location/{1}'.format(route['id'], expected['id'])
        get_location = self.simulate_request(method='GET', path=path_location, headers=auth_headers)
        self.assertEqual(200, get_location.status_code)
        data = get_location.json['data']
        actual = data[0]
        self.assertEqual('success', get_location.json['status'])
        self.assertEqual(1, len(data))
        self.assertEqual(4, len(actual))
        self.assertEqual(expected['id'], actual['id'])
        self.assertEqual(expected['latitude'], actual['latitude'])
        self.assertEqual(expected['longitude'], actual['longitude'])
        self.assertEqual(expected['dateCreate'], actual['dateCreate'])
        print('TestGetOneByIDSuccess ok')

    def test_get_one_by_id_not_found_failed(self):
        auth_headers = self.get_logged_admin_headers()
        get_list_routes = self.simulate_request(method='GET', path='/api/route', headers=auth_headers)
        route = get_list_routes.json['data'][0]

        path_location = '/api/route/{0}/location/{1}'.format(route['id'], 1500)
        get_location = self.simulate_request(method='GET', path=path_location, headers=auth_headers)
        self.assertEqual(404, get_location.status_code)
        print('TestGetOneByIDNotFoundFailed ok')

    # Insert new location

    def test_insert_success(self):
        auth_headers = self.get_logged_admin_headers()
        get_routes = self.simulate_request(method='GET', path='/api/route', headers=auth_headers)
        routes = get_routes.json['data']
        route = routes[0]
        path = '/api/route/{0}/location'.format(route['id'])
        expected = {
            "id": -1,
            "latitude": 12.2345,
            "longitude": 33.321,
            "dateCreate": "2017-04-04 12:12"
        }
        body = json.dumps(expected)
        print('Request:', body)
        result = self.simulate_request(method='POST', body=body, path=path, headers=auth_headers)
        print('Response:', result.json)
        self.assertEqual(201, result.status_code)
        data = result.json['data']
        actual = data[0]
        self.assertEqual('success', result.json['status'])
        self.assertEqual(1, len(data))
        self.assertEqual(4, len(actual))
        self.assertTrue(actual['id'] > 0)
        self.assertEqual(expected['latitude'], actual['latitude'])
        self.assertEqual(expected['longitude'], actual['longitude'])
        self.assertEqual(expected['dateCreate'], actual['dateCreate'])
        print('TestInsertSuccess ok')

    def test_insert_with_invalid_two_fields_failed(self):
        auth_headers = self.get_logged_admin_headers()
        get_routes = self.simulate_request(method='GET', path='/api/route', headers=auth_headers)
        routes = get_routes.json['data']
        route = routes[0]
        path = '/api/route/{0}/location'.format(route['id'])
        expected_location = {
            "id": 123,
            "latitude": "12.2345",
            "longitude": 33.321,
            "dateCreate": "2017-04-04 12:12"
        }
        body = json.dumps(expected_location)
        print('Request:', body)
        result = self.simulate_request(method='POST', body=body, path=path, headers=auth_headers)
        print('Response:', result.json)
        self.assertEqual(400, result.status_code)
        data = result.json['data']
        self.assertEqual(2, len(data))
        self.assertEqual('error', result.json['status'])
        print('TestInsertWithInvalidTwoFieldsFailed ok')

    def test_insert_with_invalid_value_failed(self):
        auth_headers = self.get_logged_admin_headers()
        get_routes = self.simulate_request(method='GET', path='/api/route', headers=auth_headers)
        routes = get_routes.json['data']
        route = routes[0]
        path = '/api/route/{0}/location'.format(route['id'])
        expected_location = {
            "id": 123,
            "latitude": 12.2345,
            "longitude": 33.321,
            "dateCreate": "2017-04-04 12:12"
        }
        body = json.dumps(expected_location)
        print('Request:', body)
        result = self.simulate_request(method='POST', body=body, path=path, headers=auth_headers)
        print('Response:', result.json)
        self.assertEqual(400, result.status_code)
        data = result.json['data']
        error = data[0]
        self.assertEqual('error', result.json['status'])
        self.assertEqual(error['code'], ValidateError.FIELD_INVALID_VALUE)
        self.assertEqual(error['field'], 'id')
        print('TestInsertWithInvalidIdFailed ok')

    def test_insert_with_invalid_type_failed(self):
        auth_headers = self.get_logged_admin_headers()
        get_routes = self.simulate_request(method='GET', path='/api/route', headers=auth_headers)
        routes = get_routes.json['data']
        route = routes[0]
        path = '/api/route/{0}/location'.format(route['id'])
        expected_location = {
            "id": -1,
            "latitude": "12.2345",
            "longitude": 33.321,
            "dateCreate": "2017-04-04 12:12"
        }
        body = json.dumps(expected_location)
        print('Request:', body)
        result = self.simulate_request(method='POST', body=body, path=path, headers=auth_headers)
        print('Response:', result.json)
        self.assertEqual(400, result.status_code)
        data = result.json['data']
        error = data[0]
        self.assertEqual('error', result.json['status'])
        self.assertEqual(error['code'], ValidateError.FIELD_INVALID_TYPE)
        self.assertEqual(error['field'], 'latitude')
        print('TestInsertWithInvalidTypeFailed ok')

    # Delete

    def test_delete_location_by_id_success(self):
        # get routes
        auth_headers = self.get_logged_admin_headers()
        get_list_routes = self.simulate_request(method='GET', path='/api/route', headers=auth_headers)
        route = get_list_routes.json['data'][0]
        # get locations list
        path_loc_bef = '/api/route/{0}/location'.format(route['id'])
        list_loc_bef = self.simulate_request(method='GET', path=path_loc_bef, headers=auth_headers)
        location = list_loc_bef.json['data'][0]
        loc_count_bef = len(list_loc_bef.json['data'])
        # delete location
        path_delete = '/api/route/{0}/location/{1}'.format(route['id'], location['id'])
        result = self.simulate_request(method='DELETE', path=path_delete, headers=auth_headers)
        # get locations list after delete
        path_loc_aft = '/api/route/{0}/location'.format(route['id'])
        list_loc_aft = self.simulate_request(method='GET', path=path_loc_aft, headers=auth_headers)
        list_loc_aft_json = list_loc_aft.json['data']
        loc_count_aft = len(list_loc_aft_json)
        # asserts
        self.assertEqual(204, result.status_code)
        self.assertEqual(loc_count_aft, loc_count_bef-1)
        for loc in list_loc_aft_json:
            self.assertNotEqual(loc['id'], location['id'])

    def test_delete_location_by_id_not_found_failed(self):
        # get routes
        auth_headers = self.get_logged_admin_headers()
        get_list_routes = self.simulate_request(method='GET', path='/api/route', headers=auth_headers)
        route = get_list_routes.json['data'][0]
        # delete location
        path_delete = '/api/route/{0}/location/{1}'.format(route['id'], 9999)
        result = self.simulate_request(method='DELETE', path=path_delete, headers=auth_headers)
        self.assertEqual(404, result.status_code)
        errors = result.json['data']
        error = errors[0]
        # asserts
        self.assertEqual(ValidateError.FIELD_NOT_FOUND, error['code'])
        self.assertEqual('location', error['field'])


if __name__ == '__main__':
    main()
