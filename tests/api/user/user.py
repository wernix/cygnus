from unittest import main
from core.test.testapi import TestApi


class TestUserList(TestApi):

    # Get List

    def test_get_list(self):
        result = self.simulate_request(method='GET', path='/api/user', headers=self.get_logged_admin_headers())
        data = result.json['data']
        user = data[0]
        self.assertEqual(200, result.status_code)
        self.assertNotEqual(0, len(data))
        self.assertEqual('success', result.json['status'])
        self.assertEqual(True, 'id' in user)
        self.assertEqual(True, 'login' in user)
        self.assertEqual(True, 'password' in user)
        print('TestGetList ok')

    # Get One

    def test_get_one_by_id(self):
        auth_headers = self.get_logged_admin_headers()
        get_list = self.simulate_request(method='GET', path='/api/user', headers=auth_headers)
        data = get_list.json['data']
        expected = data[1]

        one_path = '/api/user/{0}'.format(expected['id'])
        get_one = self.simulate_request(method='GET', path=one_path, headers=auth_headers)
        self.assertEqual(200, get_one.status_code)
        self.assertEqual('success', get_one.json['status'])
        data = get_one.json['data']
        actual = data[0]

        self.assertEqual(1, len(data))
        self.assertEqual(expected['id'], actual['id'])
        self.assertEqual(expected['login'], actual['login'])
        self.assertEqual(expected['dateCreate'], actual['dateCreate'])
        print('TestGetOneByID ok')

    def test_get_one_by_id_not_found_failed(self):
        auth_headers = self.get_logged_admin_headers()
        one_path = '/api/user/9999'
        get_one = self.simulate_request(method='GET', path=one_path, headers=auth_headers)
        self.assertEqual(404, get_one.status_code)
        print('TestGetOneByIDNotFoundFailed ok')


if __name__ == '__main__':
    main()
