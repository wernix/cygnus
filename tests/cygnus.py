from core.cygnus import Cygnus


cygnus = Cygnus('tests/db')
app = cygnus.api


def get_app():
    return app


def get_database():
    return cygnus.db
