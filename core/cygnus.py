import falcon
import logging.config
import os
from api.user.api import UserApi
from api.route.api import RouteApi
from core.authorization import Authorization
from core.resources import Resources
from core.db.sqlitedatabase import SqliteDatabase
from core.staticpages import StaticPages


class Cygnus(object):
    db = None
    _log_dir = 'log'
    _log_file_name = 'cygnus.log'

    def __init__(self, db_dir: str = 'db'):
        self._setup_logging()
        self.db = SqliteDatabase(db_dir)
        self._start()

    def _start(self):
        enabled_middleware = [Authorization(self.db), Resources()]
        self.api = falcon.API(middleware=enabled_middleware)

        StaticPages(self.api)
        UserApi(self.api, self.db)
        RouteApi(self.api, self.db)
        # self.staticPages = StaticPages(self.api)
        # self.userApi = UserApi(self.api, self.db)
        # self.routeApi = RouteApi(self.api, self.db)

    def _setup_logging(self):
        # create log dir if not exist
        if not os.path.isdir(self._log_dir):
            os.mkdir(self._log_dir)

        # Setup logging to file
        log_file_path = os.path.join(self._log_dir, self._log_file_name);
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            filename=log_file_path,
            filemode='w+')
        formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)
