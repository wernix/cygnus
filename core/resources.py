import logging
from pathlib import Path
from falcon import HTTPNotFound, HTTP_OK
from core.utils import resourceutils


class Resources(object):
    _log = logging.getLogger('cygnus.resources')

    def process_request(self, req, resp):

        if not req.path.startswith('/resources/'):
            return

        # Check file exists
        file_path = resourceutils.get_resource_path(req.path)
        if Path(file_path).is_file():
            self._log.debug('Resources file found %s', file_path)
        else:
            self._log.debug('Resources file not found %s', file_path)
            raise HTTPNotFound()

    def process_response(self, req, resp, resources, params):

        if not req.path.startswith('/resources/'):
            return

        file_path = resourceutils.get_resource_path(req.path)
        content_type = resourceutils.get_content_type(file_path)

        resp.stream = open(file_path, 'r')
        resp.content_type = content_type
        resp.status = HTTP_OK
