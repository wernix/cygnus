import logging


class BaseApi(object):
    _api = None
    _db = None
    _log = logging.getLogger('cygnus.api')

    def __init__(self, api, db):
        """
        :param api: Global api object
        :param db: Database object
        """
        self._log.debug('Initialize API module ...')
        self._api = api
        self._db = db
        self._setup_main()

    def _setup_main(self):
        return True

    def _add_route(self, path, main):
        self._log.debug('Registred new resources - %s', path)
        self._api.add_route(path, main)
