import logging
import sqlite3
import os
from os import errno, strerror, path, remove
from api.route.entity.location import Location
from api.route.entity.route import Route
from api.user.entity.user import User
from core.database import Database
from core.utils.dbutils import execute_from_file


class SqliteDatabase(Database):
    """
    SQLite database adapter
    """
    _db_path = None
    _db = None
    _db_res_dir = 'resources/database'
    _db_init_sql_file_name = 'sqlite.sql'
    _log = logging.getLogger('cygnus.db')

    def __init__(self, db_dir: str = 'db'):
        """
        :param db_dir: directory where the database file should be saved
        :param file_name: database file name
        """
        log = self._log

        db_path = path.join(db_dir, 'db.sqlite')

        # create default (empty) database when not exist
        if not path.exists(db_path):
            log.warning('Not found database file: %s', db_path)
            # create dir if not exist
            if not os.path.isdir(db_dir):
                os.mkdir(db_dir)
            else:
                raise Exception('Cannot create db dir: '+db_dir)

            if self._create_empty_database(db_path):
                log.debug('Created new (empty) database')
            else:
                log.error('Cannot create new (empty) database file!')
                raise FileNotFoundError(errno.ENOENT, strerror(errno.ENOENT), path)

        self._db = sqlite3.connect(db_path)
        self._db_path = db_path
        log.debug('Connected to sqlite database: %s', db_path)

    def _create_empty_database(self, db_path):
        db = sqlite3.connect(db_path)
        try:
            # create db structure
            file_init_path = os.path.join(self._db_res_dir, self._db_init_sql_file_name)
            if execute_from_file(db, file_init_path):
                return True
            else:
                raise Exception('Cannot execute script from default file')
        except all:
            if path.exists(db_path):
                remove(db_path)
            return False

    def get_file_path(self):
        return self._db_path

    def get_connection(self):
        return self._db

    def get_user_list(self, page: int = 1, limit: int = 100):
        c = self.get_connection().cursor()
        offset = (page*limit)-limit
        c.execute('SELECT id, login, date_create FROM user LIMIT ? OFFSET ?', (limit, offset))
        rows = c.fetchall()
        users = list()
        for row in rows:
            user = User(row[0], row[1], '', row[2])
            users.append(user)
        return users

    def get_user_by_id(self, user_id):
        c = self.get_connection().cursor()
        c.execute('SELECT id, login, date_create FROM user WHERE id = ?', (user_id,))
        rows = c.fetchall()
        if rows:
            row = rows[0]
            return User(row[0], row[1], None, row[2])

    def get_route_by_id(self, route_id: int):
        c = self.get_connection().cursor()
        c.execute('SELECT r.id as route_id, r.name as route_name, r.date_create as route_date_create,'
                  'u.id as user_id, u.login as user_login, u.date_create as user_date_create '
                  'FROM route as r INNER JOIN user as u ON u.id = r.user_id WHERE r.id = ?', (route_id,))
        rows = c.fetchall()
        if rows:
            row = rows[0]
            user = User(row[3], row[4], None, row[5])
            return Route(row[0], row[1], user, row[2])
        return None

    def get_route_list(self, page: int, limit: int):
        c = self.get_connection().cursor()
        offset = (page * limit) - limit
        c.execute('SELECT r.id as route_id, r.name as route_name, r.date_create as route_date_create,'
                  'u.id as user_id, u.login as user_login, u.date_create as user_date_create '
                  'FROM route as r INNER JOIN user as u ON u.id = r.user_id LIMIT ? OFFSET ?', (limit, offset))
        rows = c.fetchall()
        routes = list()
        for row in rows:
            user = User(row[3], row[4], None, row[5])
            route = Route(row[0], row[1], user, row[2])
            routes.append(route)
        return routes

    def get_location_list(self, page: int, limit: int, route_id: int):
        c = self.get_connection().cursor()
        offset = (page * limit) - limit
        c.execute('SELECT id, route_id, latitude, longitude, date_create FROM location '
                  'WHERE route_id = ? LIMIT ? OFFSET ?', (route_id, limit, offset))
        rows = c.fetchall()
        locations = list()
        for row in rows:
            location = Location(row[0], row[2], row[3], row[4])
            locations.append(location)
        return locations

    def get_location_by_id(self, location_id: int):
        c = self.get_connection().cursor()
        c.execute('SELECT id, route_id, latitude, longitude, date_create FROM location '
                  'WHERE id = ?', (location_id,))
        rows = c.fetchall()
        if rows:
            row = rows[0]
            return Location(row[0], row[2], row[3], row[4])
        return None

    def insert_location(self, loc: Location, route_id: int):
        conn = self.get_connection()
        try:
            c = conn.cursor()
            c.execute('INSERT INTO location(route_id, latitude, longitude, date_create) VALUES(?,?,?,?);',
                      (route_id, loc['latitude'], loc['longitude'], loc['dateCreate']))
            conn.commit()
            return self.get_location_by_id(c.lastrowid)
        except Exception as e:
            conn.rollback()
            self._log.exception(e)
        return None

    def delete_location_by_id(self, location_id: int):
        conn = self.get_connection()
        try:
            if self.get_location_by_id(location_id) is None:
                return False

            c = conn.cursor()
            c.execute('DELETE FROM location WHERE id = ?;', (location_id,))
            conn.commit()
            return True
        except Exception as e:
            conn.rollback()
            self._log.exception(e)
        return False
