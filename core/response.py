import falcon


class ResponseErrorData(dict):

    def __init__(self, code: str, message: str, field: str):
        super(ResponseErrorData, self).__init__()
        self['code'] = code
        self['message'] = message
        self['field'] = field


class Response(dict):
    """
    Server Response object class.
    """

    def __init__(self, response_status: str, data: list, status: str, params: dict):
        super(Response, self).__init__()
        """
        :param response_status: str with HTTP status ex. '200 Ok'
        :param status: success or error
        :param data: list with objects
        :param params: dict with using query params
        """
        self.response_status = response_status
        self['status'] = status

        if params is not None:
            self['info'] = dict()
            self['info']['page'] = params['page']
            self['info']['limit'] = params['limit']

        self.set_data(data)

    def set_data(self, data: list):
        self['data'] = data
        if 'info' in self:
            info = self['info']
            info['nextPage'] = (len(data) == info['limit'])

    @classmethod
    def bad_request(cls, data: list()):
        return Response(falcon.HTTP_BAD_REQUEST, data, 'error', None)

    @classmethod
    def created(cls, created: dict):
        return Response(falcon.HTTP_CREATED, [created], 'success', None)

    @classmethod
    def not_found(cls, data: list):
        return Response(falcon.HTTP_NOT_FOUND, data, 'error', None)

    @classmethod
    def internal_error(cls):
        return Response(falcon.HTTP_INTERNAL_SERVER_ERROR, None, None, None)

    @classmethod
    def ok(cls, data: list, params: dict):
        return Response(falcon.HTTP_OK, data, 'success', params)

    @classmethod
    def deleted(cls):
        return Response(falcon.HTTP_NO_CONTENT, None, None, None)
