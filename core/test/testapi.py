import json
import os
from falcon import testing
from tests import cygnus, mockdata


class TestApi(testing.TestCase):
    app = None
    db = None

    def get_logged_admin_headers(self):
        admin = mockdata.admins[0]
        body = json.dumps(admin)
        login_result = self.simulate_request(method='POST', path='/login', body=body)
        cookie = login_result.cookies['SessionID']
        return {
            'Cookie': cookie.name + '=' + cookie.value
        }

    def setUp(self):
        super(TestApi, self).setUp()
        print('\n================\n',
              self.__class__.__name__, 'TestCase',
              '\n================\n')
        self._prepare_database()
        self.app = cygnus.get_app()
        self.db = cygnus.get_database()

    def _prepare_database(self):
        conn = cygnus.get_database().get_connection()
        c = conn.cursor()
        try:
            self._clear_database()

            # select data to inserting
            user_mock = (mockdata.admins,mockdata.users)
            route_mock = (mockdata.routes,)
            location_mock = (mockdata.locations_route_1,)

            # import all user mock data
            for mock in user_mock:
                for u in mock:
                    c.execute('INSERT INTO user(id, login, password, date_create) VALUES(?,?,?,?)',
                              (u['id'], u['login'], u['password'], u['date_create']))

            # import all route mock data
            for mock in route_mock:
                for r in mock:
                    c.execute('INSERT INTO route(id, name, user_id, date_create) VALUES(?,?,?,?)',
                              (r['id'], r['name'], r['user_id'], r['date_create']))

            # import all location mock data
            for mock in location_mock:
                for l in mock:
                    c.execute('INSERT INTO location(id, route_id, latitude, longitude, date_create) VALUES(?,?,?,?,?)',
                              (l['id'], l['route_id'], l['latitude'], l['longitude'], l['date_create']))

            conn.commit()
        except Exception as e:
            print(e)
            self.fail('Cannot prepare database')

        print('Prepared database.')

    def tearDown(self):
        self._clear_database()
        super(TestApi, self).tearDown()

    def _clear_database(self):
        tables = ('route', 'user', 'location')
        conn = cygnus.get_database().get_connection()
        c = conn.cursor()
        for name in tables:
            c.execute('DELETE FROM '+name)
            c.execute('DELETE FROM sqlite_sequence WHERE name=?;', (name,))
        conn.commit()
        print('Cleared database')
