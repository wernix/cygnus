
class BaseApiMain(object):

    _db = None
    _params = dict()

    def __init__(self, db):
        """
        :param db: Database object
        """
        self._db = db

    def on_get(self, req, resp):
        self._params['limit'] = int(req.params['limit']) if 'limit' in req.params else 50
        self._params['page'] = int(req.params['page']) if 'page' in req.params else 1
        pass

    def on_post(self, req, resp):
        pass

    def on_delete(self, req, resp):
        pass
