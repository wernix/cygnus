import logging
from pathlib import Path
from falcon import HTTP_OK, HTTP_NOT_FOUND
from mimetypes import MimeTypes
from core.utils import resourceutils


class StaticPages(object):
    _log = logging.getLogger('cygnus.resources')

    def __init__(self, app):
        log = self._log

        log.debug('Registred static resource /login')
        app.add_route('/login', LoginStaticPage())
        log.debug('Registred static resource /logout')
        app.add_route('/logout', LogoutStaticPage())
        log.info('Initialized static resources.')


class LoginStaticPage(object):
    _log = logging.getLogger('cygnus.resources')

    def on_get(self, req, resp):
        path = '/resources/login/login.html'
        file_path = resourceutils.get_resource_path(path)
        if Path(file_path).is_file():
            resp.stream = open(file_path, 'r')
            resp.content_type = MimeTypes().guess_type(file_path)[0]
            resp.status = HTTP_OK
        else:
            self._log.error('Not found default login page!', file_path)
            resp.status = HTTP_NOT_FOUND

    def on_post(self, req, resp):
        # All operation include in authorization middleware
        pass


class LogoutStaticPage(object):

    def on_get(self, req, resp):
        # All operation include in authorization middleware
        pass

    def on_post(self, req, resp):
        # All operation include in authorization middleware
        pass
