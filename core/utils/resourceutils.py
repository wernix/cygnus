from pathlib import Path
from os import getcwd
from mimetypes import MimeTypes


def get_resource_path(path):
    internal_file_path = getcwd() + path.replace('/resources/', '/resources/htdocs/internal/')
    public_file_path = getcwd() + path.replace('/resources/', '/resources/htdocs/public/')

    if Path(internal_file_path).is_file():
        file_path = internal_file_path
    else:
        file_path = public_file_path

    return file_path


def get_file_ext(file_path):
    file_name, file_ext = file_path.splitext(file_path)
    return file_ext


def get_content_type(file_path):
    ct = MimeTypes().guess_type(file_path)[0]
    if ct is None:
        file_ext = get_file_ext(file_path)
        if file_ext == '.woff2':
            ct = 'application/font-woff'

    return ct
