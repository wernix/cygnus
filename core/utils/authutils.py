import hashlib
from base64 import b64decode, b64encode
from binascii import hexlify
from os import urandom
from datetime import datetime, date


def get_new_session_id():
    return hexlify(urandom(20)).decode()


def sha512_encode(text: str):
    return hashlib.sha512(text.encode()).hexdigest()


def authorization_base_decode(text):
    d = b64decode(text).decode()
    r = d.split(':')
    c = dict()
    c['username'] = r[0]
    c['password'] = r[1]
    return c


def authorization_base_encode(username, password):
    return b64encode(username+':'+password)


def set_new_cookie_to_response(resp, session_id: str):
    today = date.today()
    resp.set_cookie(
        name='SessionID',
        value=session_id,
        expires=datetime(
            year=today.year,
            month=today.month,
            day=today.day,
            hour=23,
            minute=59
        ),
        http_only=False,
        max_age=0,
        path='/',
        secure=False
    )
