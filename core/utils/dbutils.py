
def execute_from_file(db, file_path):
    c = db.cursor()
    try:
        # all content from sql file execute in sqlite
        file = open(file_path)
        script = ''
        for line in file:
            script += line
        c.executescript(script)
        # save changes
        db.commit()
        return True
    except all:
        db.rollback()
        return False
