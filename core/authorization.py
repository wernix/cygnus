import json
import logging

from falcon import HTTPFound, HTTPUnauthorized

from core.auth.basicauth import BasicAuth
from core.sessionmanager import SessionManager
from core.utils import authutils


class Authorization(object):
    """
    Authorization class
    """
    _session_id = None
    _session_manager = None
    _db = None
    _log = logging.getLogger('cygnus.auth')

    def __init__(self, database):
        """
        :param database: Database object
        """
        self._session_manager = SessionManager()
        self._db = database

    def process_request(self, req, resp):
        log = self._log

        # Not all request may be authorized
        if not self._is_request_for_auth(req):
            return True

        is_logout_process = req.path.lower().startswith('/logout') and (req.method in ['POST', 'GET'])
        if is_logout_process:
            self._remove_session(req)
            return True

        if 'SessionID' not in req.cookies:
            self._create_session(resp)

        is_login_process = req.path.lower().startswith('/login') and (req.method == 'POST')
        # Login by incoming POST request
        if is_login_process:
            if self._try_auth_by_request(req, resp):
                return True
            else:
                self._remove_session(req)
                return False

        # Login by cookies
        if self._try_auth_by_cookie(req):
            log.debug('[%s] request authorized by cookie [%s]',
                      req.path, req.cookies['SessionID'])
            return True

        # When user log by www-authenticate header
        if self._try_auth_by_header(req):
            log.debug('[%s] request authorized by header %s',
                      req.path, req.auth)
            return True

        # IF not auth - unauthorized
        log.info('[%s] request unauthorized.', req.path)
        self._close_response(req, resp)

    def _try_auth_by_cookie(self, req):
        if 'SessionID' in req.cookies:
            session_id = req.cookies['SessionID']
            if session_id is not None:
                if self._session_manager.is_logged(session_id):
                    self._session_id = session_id
                    return True
        return False

    def _try_auth_by_header(self, req):
        auth = req.auth
        if auth is not None:
            basic = BasicAuth(auth)
            login = basic.username
            password = basic.password
            password_sha512 = authutils.sha512_encode(password)
            if self._user_auth_is_valid(login, password_sha512):
                self._session_manager.create(self._session_id, basic.username)
                return True
        return False

    def _try_auth_by_request(self, req, resp):
        body = req.stream.read().decode('utf-8')
        body_json = json.loads(body)
        login = body_json['login']
        password = body_json['password']
        if self._user_auth_is_valid(login, password):
            self._auth_request(login, resp)
            self._log.debug('[%s] request authorized by POST request [%s]',
                            req.path, login)
            return True
        else:
            self._log.debug('[%s] request NOT authorized by POST request [%s]',
                            req.path, login)
            return False

    def _is_request_for_auth(self, req):
        # Not authorize requests to resources sent from login page
        is_login_resource = req.path.lower().startswith('/resources/login') and ('REFERER' in req.headers)
        is_login_page = req.path.lower().startswith('/login') and (req.method == 'GET')

        if is_login_resource:
            referer = req.headers['REFERER'].lower()
            is_login_req = (referer.find('/login') > -1) and (req.method == 'GET')
            return is_login_req is False
        elif is_login_page:
            return False

        return True

    def _user_auth_is_valid(self, username, password):
        c = self._db.get_connection().cursor()
        c.execute('SELECT password FROM user WHERE login = ?', (username,))
        result = c.fetchone()

        if result is not None:
            if result[0].lower() == password.lower():
                return True

        return False

    def _close_response(self, req, resp):
        is_login_process = req.path.lower().startswith('/login') and (req.method == 'POST')

        if is_login_process:
            raise HTTPUnauthorized()
        else:
            # Unauthorized request redirect to login page
            redirect_to = '/login'
            path = req.path
            if not path.lower().startswith('/resources'):
                redirect_to += '?r='+path
            raise HTTPFound(redirect_to)

    def _auth_request(self, login, resp):
        new_session_id = authutils.get_new_session_id()
        authutils.set_new_cookie_to_response(resp, new_session_id)
        self._session_manager.create(new_session_id, login)

    def _create_session(self, resp):
        new_session_id = authutils.get_new_session_id()
        authutils.set_new_cookie_to_response(resp, new_session_id)
        self._session_id = new_session_id

    def _remove_session(self, req):
        if 'SessionID' in req.cookies:
            self._session_manager.delete(req.cookies['SessionID'])
