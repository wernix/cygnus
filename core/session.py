
class Session(object):
    """
    Session information
    """

    def __init__(self, session_id, login):
        """
        :param session_id: Session ID
        :param login: Current user login
        """
        self.login = login
        self.session_id = session_id
