from core.utils import authutils


class BasicAuth:
    """ Class decode / encode basic auth """
    username = None
    password = None

    def __init__(self, auth):
        """
        :param auth: Authorization header value
        """
        e = auth.replace('Basic ', '')
        c = authutils.authorization_base_decode(e)
        self.username = c['username']
        self.password = c['password']

    def get_encoded(self):
        return authutils.authorization_base_encode(self.username, self.password)
