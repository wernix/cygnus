import logging
from core.session import Session


class SessionManager(object):
    _log = logging.getLogger('cygnus.session')
    """
    SessionManager class.
    """

    def __init__(self):
        self._logged = dict()

    def is_logged(self, session_id: str):
        """
        :param session_id: SessionID from cookie.
        :return: True if session_id is logged or False.
        """
        for key, value in self._logged.items():
            if key == session_id:
                return True

        return False

    def get_session(self, session_id: str):
        """
        :param session_id: SessionID from cookie.
        :return: Session object if logged.
        """
        return self._logged[session_id]

    def create(self, session_id: str, login: str):
        """
        :param session_id: Authorized SessionID cookie.
        :param login: Authorized user login.
        :return: new Session object.
        """
        self._log.debug('Create new logged session [%s] [%s]', login, session_id)
        new_session = Session(session_id, login)
        self._logged[session_id] = new_session
        # self._logged.append(new_session)
        return new_session

    def delete(self, session_id: str):
        """
        :param session_id: SessionID from cookie.
        """
        if session_id in self._logged:
            s = self._logged[session_id]
            del self._logged[session_id]
            self._log.debug('Removed logged session [%s] [%s]', s.login, s.session_id)
