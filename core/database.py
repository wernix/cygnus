from api.route.entity.location import Location


class Database(object):

    def __init__(self, db_file = None):
        pass

    def get_connection(self):
        pass

    # Route

    def get_route_by_id(self, route_id: int):
        pass

    def get_route_list(self, page: int, limit: int):
        pass

    # Location

    def insert_location(self, loc: Location, route_id: int):
        pass

    def get_location_by_id(self, location_id: int):
        pass

    def delete_location_by_id(self, location_id: int):
        pass


class NotFoundError(Exception):

    def __init__(self, message):
        super(NotFoundError, self).__init__(message)
