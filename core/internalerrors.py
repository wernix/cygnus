from core.response import ResponseErrorData


class ValidateError:
    FIELD_INVALID_VALUE = 'cygnus.validate.1000'
    FIELD_NOT_FOUND = 'cygnus.validate.1001'
    FIELD_INVALID_TYPE = 'cygnus.validate.1002'


class ValidateResponseErrorData(ResponseErrorData):

    @classmethod
    def field_invalid_value(cls, field: str, detail: str):
        return ValidateResponseErrorData(code=ValidateError.FIELD_INVALID_VALUE,
                                         message=detail,
                                         field=field)

    @classmethod
    def field_not_found(cls, field: str):
        return ValidateResponseErrorData(code=ValidateError.FIELD_NOT_FOUND,
                                         message='Not found object in database',
                                         field=field)

    @classmethod
    def field_invalid_type(cls, field: str, expected: str):
        return ValidateResponseErrorData(code=ValidateError.FIELD_INVALID_TYPE,
                                         message='Field type is incorrect - expected {0}'.format(expected),
                                         field=field)
