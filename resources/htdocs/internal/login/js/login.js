function setupLoginForm() {
    // set focus on login field after load page
    document.addEventListener('DOMContentLoaded', setFocusInLogin, false);
    // when press key enter/return press enter sign in button
    document.addEventListener('keypress', loginFormKeyPressEvent, false);
}

function setFocusInLogin() {
    document.getElementById('login').focus();
}

function signInProcess() {
    if (loginFormIsComplete())
        sendAuthData()
}

function loginFormIsComplete() {
    let loginForm = document.getElementById('form-login')
        fieldLogin = document.getElementById('login'),
        fieldPassword = document.getElementById('password');

    if (loginForm.checkValidity()) {
        return true;
    }else {
        if (!fieldLogin.checkValidity()) {
            fieldLogin.focus();
        }else if (!fieldPassword.checkValidity()) {
            fieldPassword.focus()
        }
        return false;
    }
}

function sendAuthData() {
    let http = new XMLHttpRequest(),
        fieldLogin = document.getElementById('login'),
        fieldPassword = document.getElementById('password'),
        SHA512 = new Hashes.SHA512,
        body = {
            login: fieldLogin.value,
            password: SHA512.hex(fieldPassword.value)
        };

    http.open('POST', '/login', true);
    http.setRequestHeader('Content-type', 'application/json');
    http.onreadystatechange = function() {
        if(http.readyState == 4) {
            if (http.status == 200)
                onSignInSuccess(http)
            else
                onSignInFailed(http);

        }
    };
    http.onerror = function() {
        alert('Sign in error');
    };
    http.ontimeout = function() {
        alert('Connection timeout. Try again.');
    };
    http.send(JSON.stringify(body));
}

function onSignInSuccess(http) {
    if (window.location.search.indexOf('r=') > -1) {
        let r = window.location.search.split('r=')[1],
            h = window.location.origin,
            path = h+r;
        console.log('success, redirect to: ', path);
        window.location = path;
    }else {
        console.log('success, redirect to default')
        window.location = window.location.origin;
    }
}

function onSignInFailed(http) {
    switch(http.status) {
        case 401:
            alert('Unauthorized. invalid login or password.')
            break;
        default:
            alert('Unknown error. Try again.');
    }
}

function loginFormKeyPressEvent(event) {
    let keyCode = event.keyCode,
        active = document.activeElement,
        loginInput = document.getElementById('login'),
        passwordInput = document.getElementById('password'),
        signInButton = document.getElementsByClassName('login-button')[0];

    if (keyCode == 13) {
        if (active == loginInput) {
            passwordInput.focus()
        }else if (active == passwordInput) {
            signInButton.click()
        }
    }
}

setupLoginForm();