CREATE TABLE user (
    id          INTEGER        PRIMARY KEY AUTOINCREMENT
                               UNIQUE,
    login       VARCHAR (100)  NOT NULL
                               UNIQUE,
    password    VARCHAR (1000) NOT NULL,
    date_create DATETIME       NOT NULL
);
CREATE TABLE route (
    id          INTEGER       PRIMARY KEY AUTOINCREMENT
                              UNIQUE,
    name        VARCHAR (100) UNIQUE
                              NOT NULL,
    user_id     INTEGER       REFERENCES user (id)
                              NOT NULL,
    date_create DATETIME      NOT NULL
);
CREATE TABLE location (
    id          INTEGER  PRIMARY KEY AUTOINCREMENT
                         UNIQUE,
    route_id    INTEGER  REFERENCES route (id)
                         NOT NULL,
    latitude    NUMERIC  NOT NULL,
    longitude   NUMERIC  NOT NULL,
    date_create DATETIME NOT NULL
);